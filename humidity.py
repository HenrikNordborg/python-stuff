# -*- coding: utf-8 -*-
"""
Created on Fri Aug  7 11:58:42 2020

@author: Henrik
"""

import numpy as np

def pvapor( T ):
  A = 8.07131
  B = 1730.63
  C = 233.426
  K = 101325/760
  p = K*10**(A-B/(C+T))
  return p

def Tdew(p):
  A = 8.07131
  B = 1730.63
  C = 233.426
  K = 101325/760
  T = B/(A - np.log(p/K)) - C
  return T



fout = 0.5
Tout=  23

mwater = 14

Tin = 15

pin = pvapor(Tin)
pout = pvapor(Tout)

fex = pin/pout

dp = (fout - fex)*pout

# Compute the amount of water per m^3

dn = dp*1.0/(8.314*(273.15+Tout))

dm = dn*18e-3

volume = mwater/dm

tvec = np.linspace(Tin,30,100)
fvec = np.zeros( tvec.shape )

for k in range(0,tvec.size):
  fvec[k] = pin/pvapor(tvec[k])
  
import matplotlib.pyplot as plt

titletext = 'Critical outside humidity at Tin = %f' % Tin 

plt.figure(figsize=(16,9))
plt.title( titletext )
plt.grid()
plt.plot(tvec,fvec,label='f_r')
plt.xlabel('Outside temperature in °C')
plt.ylabel('Relative humidity')
plt.show() 