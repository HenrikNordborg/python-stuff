# -*- coding: utf-8 -*-
"""
Created on Sat Dec 19 13:14:35 2020

@author: Henrik
"""

# Calculate all the possible outcomes in Risk

import numpy as np

num_sides = 6
max_num_dice = 6
max_results = num_sides ** max_num_dice

# Temporary workspace
outcomes = np.zeros((max_results,max_num_dice),dtype = int) 
tmpoutcome = np.zeros(max_num_dice,dtype = int)
oc_cnt = 0

def count_states( numsides, level, maxv ):
  
  cnt = 0
  
  if level > 1:
    
    for k in range(0,maxv):
      
      cnt += count_states(numsides,level-1,maxv-k)
      
  else:
    
    cnt = maxv
    
  # print( level, maxv, cnt )    
  return cnt
 

def build_table( num_sides, num_dice ):
  
  global counter
  
  counter = 0
  btrec( num_sides, num_dice, 1 )
  
  # Extract the relevant part of the table
  table = np.array( outcomes[0:counter,0:num_dice] )
  
  # Computer the statistical weight of each outcome

  weight = np.zeros( counter, dtype = int )
  weightsum = 0
  perms = np.math.factorial( num_dice )
  kp = np.zeros( num_sides, dtype = int )
  
  for k in range( 0, counter ):
    
    kp[0:num_sides] = 0
    tmp = perms
    
    for q in range(0,num_dice):
      
      d = table[k][q]
      kp[d-1] += 1
      
    for q in range(0,num_sides):
      
      tmp /= np.math.factorial( kp[q] )
      
    weight[k] = tmp
    weightsum += weight[k]
 
  # print( table )
  # print( weight )
  
  probs = np.zeros(counter)
  p0 = 1.0/( num_sides ** num_dice  )
  
  for k in range( 0, counter ):
    
    probs[k] = p0*weight[k]
    
  # print( 'Counter = ', counter)
  # print( 'Number of outcomes', weightsum )
  return table, probs  
  

def btrec ( ns, nd, lev ):
  
  global counter
  global tmpoutcome
  global outcomes
  
  if lev == 1:
    dmax = ns
  else:
    dmax = tmpoutcome[lev-2]     
  
  if lev < nd: 
      
    for k in range(dmax,0,-1):
      
      tmpoutcome[lev-1] = k
      btrec(ns,nd,lev+1)
      
  else: 
   
    for k in range(dmax,0,-1):
    
      tmpoutcome[lev-1] = k
      # print(tmpoutcome)
      outcomes[counter] = tmpoutcome
      counter += 1
      

def evaluate_attack ( num_sides, ad, dd ):
  
  atbl,ap = build_table( num_sides, ad )
  dtbl,dp = build_table( num_sides, dd )
  
  result_table = np.zeros((3,3))
  
  acnt = atbl.shape[0]
  dcnt = dtbl.shape[0]
  
  dmin = np.min((ad,dd))

  for ka in range(0,acnt):
          
    for kd in range(0,dcnt):
      
      awin = 0
      dwin = 0
      
      for q in range(0,dmin):
        
        if atbl[ka][q] > dtbl[kd][q]:
            
          awin += 1
          
        else:
          
          dwin += 1
          
      result_table[awin][dwin] += ap[ka]*dp[kd]
      
  
  # Test normalization
  res = 0
  
  for ka in range(0,3):
          
    for kd in range(0,3):
      
      res += result_table[ka][kd]
  
  # print( 'res = ', res )
  
  print('===========================') 
  print('Risk results')  
  print( 'Attacker with %d dice' % ad )
  print( 'Defender with %d dice' % dd )

  print( 'A2/D0: %3.2f' % result_table[2][0])
  print( 'A1/D0: %3.2f' % result_table[1][0])
  print( 'A1/D1: %3.2f' % result_table[1][1])
  print( 'A0/D1: %3.2f' % result_table[0][1])
  print( 'A0/D2: %3.2f' % result_table[0][2])
  
    
evaluate_attack( 6, 3, 2 )
evaluate_attack( 6, 2, 2 )
evaluate_attack( 6, 3, 1 )
evaluate_attack( 6, 2, 1 )
evaluate_attack( 6, 1, 2 )
evaluate_attack( 6, 1, 1 )
  


# count_states( 6, 2, 6 )

  
  