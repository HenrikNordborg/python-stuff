# -*- coding: utf-8 -*-
"""
Created on Thu May 21 16:31:54 2020

@author: Henrik
"""

import numpy as np
import matplotlib.pyplot as plt

def lsqcond( A, y, cond, fc ):
  
  At = A.transpose()
  AA = np.dot(At,A)
  yp = np.dot(At,y)
  
  nrows = A.shape[0]
  ncoeffs = A.shape[1]
  ncond  = cond.shape[0]
  ntot = ncoeffs + ncond
  
  M = np.zeros((ntot,ntot))
  ym = np.zeros(ntot)
 
  M[0:ncoeffs,0:ncoeffs] = AA
  M[ncoeffs:ntot,0:ncoeffs] = cond
  M[0:ncoeffs,ncoeffs:ntot] = cond.transpose()
  ym[0:ncoeffs] = yp
  ym[ncoeffs:ntot] = fc
 
  c = np.linalg.solve(M,ym)
  cm = c[0:ncoeffs]
  
  return cm

def testfunc( x, d ):
  Ac = 2
  bc = 1
  
  if d == 0:
    res = Ac*( 1 - np.exp(-bc*x) )
  else:
    res = Ac*bc*np.exp(-bc*x)
  return res

npts = 20
xmax = 5
dx = xmax / npts

x = np.arange(0,xmax+0.5*dx,dx)
y = np.zeros(x.shape)
ya = np.zeros(x.shape)
yf = np.zeros(x.shape)

Ac = 2
bc = 1

for k in range(0,x.size):
  y[k] = testfunc(x[k],0)
  ya[k] = y[k] + ( np.random.random_sample() - 0.5 )


ord = 4
nrows = x.size
ncols = ord + 1

A = np.zeros((nrows,ncols))
for m in range(0,nrows):
  for n in range(0,ncols):
    A[m][n] = np.power(x[m],ord-n)

# Set the conditions
cond = np.zeros((4,ncols))
fc = np.zeros((4))

x1 = x[0]
x2 = x[-1]

for k in range(0,ncols):
  cond[0,k] = np.power(x1,ord-k)
  cond[2,k] = np.power(x2,ord-k)

for k in range(0,ncols-1):
  cond[1,k] = (ord-k)*np.power(x1,ord-k-1)
  cond[3,k] = (ord-k)*np.power(x2,ord-k-1)
                
fc[0] = testfunc(x1,0)
fc[1] = testfunc(x1,1)
fc[2] = testfunc(x2,0)
fc[3] = testfunc(x2,1)                

                

cf = lsqcond(A,y,cond,fc)

pf = np.poly1d(cf)
pfder = pf.deriv()

print('%f %f\n' % (pf(x1), testfunc(x1,0))   )
print('%f %f\n' % (pfder(x1), testfunc(x1,1))   )
print('%f %f\n' % (pf(x2), testfunc(x2,0))   )
print('%f %f\n' % (pfder(x2), testfunc(x2,1))   )




for k in range(0,x.size):
  yf[k] = pf(x[k])


plt.figure(1,figsize=(16,9))
plt.title('Plot')
plt.grid()
plt.plot(x,ya,'o',label='Data')
plt.plot(x,y,'-',label='Exact') 
plt.plot(x,yf,'-',label='fit')
plt.xlabel('x')
plt.ylabel('y')
plt.legend()
plt.show() 



