# -*- coding: utf-8 -*-
"""
Created on Sat Dec 19 07:24:19 2020

@author: Henrik
"""

import numpy as np
from datetime import datetime

def fit_with_polynomial( data, days ):
  
  x = data[0][-days:]
  y = data[1][-days:]
  xf = np.zeros( y.shape )
  yf = np.zeros( y.shape )
  
  d1 = x[0].toordinal()
  for k in range(0,len(x)):
    xf[k] = x[k].toordinal() - d1
    
  print('\n',x)
  print('\n',y)
  print('\n',xf)
  print('\n',yf)
  
  cp = np.polyfit( xf, y, 2 )
  yf = np.polyval( cp, xf )
  
  return cp, x, yf

cp,xf,yf = fit_with_polynomial( data, 21 )