"""
Created on Wed Mar 25 06:48:20 2020

The data is taken from

https://data.humdata.org/dataset/novel-coronavirus-2019-ncov-cases

@author: Henrik

"""

import csv
import numpy as np
from datetime import datetime
import matplotlib.pyplot as plt
import requests

plt.rcParams.update({'font.size': 18})
deathcut = 10
tfitmax = 50

def read_deaths():
  
  url = ("https://data.humdata.org/hxlproxy/api/data-preview.csv?"
         "url=https%3A%2F%2Fraw.githubusercontent.com"
         "%2FCSSEGISandData%2FCOVID-19%2Fmaster%2Fcsse_covid_19_data"
         "%2Fcsse_covid_19_time_series%2Ftime_series_covid19_deaths_global.csv"
         "&filename=time_series_covid19_deaths_global.csv")
  
  # Read everything into the array data
  data = []
  # with open('time_series_covid19_deaths_global.csv') as File:
  #   reader = csv.reader(File)
  #   for row in reader:
  #     data.append(row)
  
  try:
    response = requests.get(url)
  except:
    print('Error: Unable to obtain data')
  
  reader = csv.reader(response.text.strip().split('\n'))
  for row in reader:
    data.append(row)

  # Convert the dates into ordinals. This is not strictly 
  # necessary.
  npts = len(data[0])

  for k in range(4,npts):
    tmp = datetime.strptime(data[0][k],'%m/%d/%y')
    # data[0][k] = tmp.toordinal()
    data[0][k] = tmp

  return data

def read_confirmed():
  
  
  url = (" https://data.humdata.org/hxlproxy/api/data-preview.csv?"
         "url=https%3A%2F%2Fraw.githubusercontent.com"
         "%2FCSSEGISandData%2FCOVID-19%2Fmaster%2Fcsse_covid_19_data"
         "%2Fcsse_covid_19_time_series"
         "%2Ftime_series_covid19_confirmed_global.csv"
         "&filename=time_series_covid19_confirmed_global.csv" )
  
  # Read everything into the array data
  data = []
  # with open('time_series_covid19_deaths_global.csv') as File:
  #   reader = csv.reader(File)
  #   for row in reader:
  #     data.append(row)
  
  try:
    response = requests.get(url)
  except:
    print('Error: Unable to obtain data')
  
  reader = csv.reader(response.text.strip().split('\n'))
  for row in reader:
    data.append(row)

  # Convert the dates into ordinals. This is not strictly 
  # necessary.
  npts = len(data[0])

  for k in range(4,npts):
    tmp = datetime.strptime(data[0][k],'%m/%d/%y')
    data[0][k] = tmp.toordinal()

  return data


def get_country( cname, data ):

  npts = len(data) - 1
  # dvec = np.array(data[0][4:],dtype = float)
  dvec = np.array(data[0][4:])
  
  # Extract data belonging to one country
  cnt = 1
  flag = 0
  while flag > -1:
    
    if data[cnt][1] == cname or cname == 'World':
      
      if flag == 0:
        
        deaths = np.array(data[cnt][4:],dtype = float)
        flag = 1
        
      elif flag == 1:
        
        deaths = deaths + np.array(data[cnt][4:],dtype = float)
      
    else:
      
      if flag == 1:
        flag = -1
        
    if cnt == npts:
      # print(data[cnt][1])
      break
    
    cnt += 1

    
  # Remove small values
  cnt = 0
  while deaths[cnt] < deathcut:
    cnt += 1
    
  dvec = dvec[cnt:]
  deaths = deaths[cnt:]
  
  return np.array( ( dvec, deaths) )


def polyval( c, x ):
  
  sum = ((c[3]*x + c[2])*x + c[1])*x + c[0]
  return sum       


def compute_diffs( data ):
  npts = data.shape[1]-1
  res = np.array((data[0][1:],data[1][1:]))
  for k in range (0,npts):
    res[0][k] = data[0][k+1]
    res[1][k] = data[1][k+1] - data[1][k]
  
  return res

def smoothed_daily_cases( data ):
  
  # Compute the daily deaths from the differences
  dd = compute_diffs( data )

  # Obtain the number of data points
  ldd = len(dd[0])


  # Determin the averaging inteval
  days = 21
  d1 = days // 2
  d2 = days - d1
  
  # Remove the boundaries
  dts = dd[0][d1:ldd-d2+1]
  deaths = dd[1][d1:ldd-d2+1]
  
  npts = len(dts)
  res = np.array((dts,deaths))
     
  for k in range(0,npts):
    q1 = k
    q2 = k + days
    cnt = q2 - q1
    #cnt = 0
    sum = 0
    for q in range(q1,q2):
      sum += dd[1][q]
      #cnt += 1

    res[1][k] = sum/cnt
    #print(cnt,sum,dd[0][q1],dd[0][q2-1])
    
  return res

def check_country( cname, deathdata ):
  
  dtot = get_country( cname, deathdata )
  ddaily = compute_diffs( dtot )
  dsmoothed = smoothed_daily_cases( dtot )
  return dtot, ddaily, dsmoothed
  


# clist = ('Sweden','Switzerland','Austria','Germany','Italy',
#          'US','United Kingdom','Netherlands', 'Denmark', 'France',
#          'Norway')

clist = ['Sweden','Switzerland','Austria','Netherlands','Germany',
         'Italy','US','United Kingdom']

clist.sort()


plist = { 'Sweden' : 10120195,'Switzerland' : 8675630,'Austria' : 9023489,
         'Germany' : 83873315,'Italy' : 60432001, 'US' : 331648064, 
         'United Kingdom' : 68004678, 'Romania' : 19193685, 
         'Netherlands' : 17148361, 'Denmark' : 5799949, 'France' : 65328442,
         'Norway' : 5437616, 'Poland' : 37830343, 'Brazil' : 213147624,
         'Czechia' : 10716756, 'Spain' : 46762418  }
  

# deathdata = read_deaths()
if not 'deathdata' in locals(): 
  deathdata = read_deaths()
  print('Data from humdata.org have been imported')

plt.figure(figsize=(16,9),dpi=150)
plt.title('Daily (total) deaths per million')
plt.grid()

flag = 0
for country in clist:
 
  rawdata = get_country(country,deathdata)

  if flag == 0:
    print('Last date = ',rawdata[0][-1])
    flag = 1

  data = smoothed_daily_cases( rawdata )
  pop = plist[country]
  data[1] *= 1.0e6 / pop
  totaldeaths = rawdata[1][-1] * 1.0e6/pop
  # print (country, pop, totaldeaths)
  
  labeltext = country + ' %4.2f (%2.0f)' % ( data[1][-1],totaldeaths)
  print( labeltext )
  plt.plot(data[0],data[1],'-',label=labeltext)


plt.ylabel('Daily deaths')
plt.xlabel('Date')


plt.legend(loc=9,fontsize = 12)
#plt.text(posx,posy,mytext)
plt.show() 

plt.figure(figsize=(16,9),dpi=150)
plt.title('Deaths per million')
plt.grid()

flag = 0


for country in clist:
 
  rawdata = get_country(country,deathdata)

  data = np.array( rawdata )
  pop = plist[country]
  data[1] *= 1.0e6 / pop
  totaldeaths = data[1][-1]
  # print (country, pop, totaldeaths)
  
  labeltext = country + ' %2.0f' % totaldeaths
  # print( labeltext )
  plt.plot(data[0],data[1],'-',label=labeltext)


plt.ylabel('Daily deaths')
plt.xlabel('Date')

plt.legend(fontsize = 12)
#plt.text(posx,posy,mytext)
plt.show() 



tmp,dtmp,stmp = check_country('Sweden',deathdata)








