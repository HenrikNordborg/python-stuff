# -*- coding: utf-8 -*-
"""
Created on Sat Jul 11 11:51:51 2020

@author: Henrik
"""

# Create two datasest for trying Gaussion extrapolationx

import numpy as np
import matplotlib.pyplot as plt
import scipy
from scipy.spatial.distance import cdist
from scipy.stats import multivariate_normal

def exponentiated_quadratic(xa, xb): 
  #   """Exponentiated quadratic  with σ=1"""
  # L2 distance (Squared Euclidian)
  sq_norm = -0.5 * cdist(xa, xb, 'sqeuclidean')
  return np.exp(sq_norm)

def GP_noise(X1, y1, X2, kernel_func, sigma_noise, n1):
  """
  Calculate the posterior mean and covariance matrix for y2
  based on the corresponding input X2, the noisy observations 
  (y1, X1), and the prior kernel function.
  """
  # Kernel of the noisy observations
  sigm11 = kernel_func(X1, X1) + sigma_noise * np.eye(n1)
  # Kernel of observations vs to-predict
  sigm12 = kernel_func(X1, X2)
  print('sig12 shape',sigm12.shape)
  # Solve
  solved = scipy.linalg.solve(sigm11, sigm12, assume_a='pos').T
  # Compute posterior mean
  mu2 = solved @ y1
  # Compute the posterior covariance
  sigm22 = kernel_func(X2, X2)
  sigm2 = sigm22 - (solved @ sigm12)
  return mu2, sigm2  # mean, covariance



t1max = 10
t2max = 20
npts = 10


t1 = np.linspace( 0, t1max, npts, endpoint = False )
t2 = np.linspace(t1max, t2max, npts+1)


X1 = t1
X2 = t2

Y1 = t1*( 1 + 0.2*np.sin(t1) )


plt.figure(figsize=(16,9))
plt.title('Inputdata')
plt.grid()
plt.plot(t1,X1,label='X1')
plt.plot(t1,Y1,label='Y1')
plt.plot(t2,X2,label='X2')
#plt.xlabel('Daily deaths')
#plt.text(posx,posy,mytext)
plt.show() 

