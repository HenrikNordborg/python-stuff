# -*- coding: utf-8 -*-
"""
Created on Mon Dec 28 10:06:51 2020

@author: Henrik
"""

import numpy as np
import matplotlib.pyplot as plt

# Wie ich immer in Roulette gewinne

# Wahrscheinlichkeiten
p = 18/37

# Einsatz
a = 1


def simulation( p, a ):
  
  einsatz = 0
  gewinn = 0
  
  maxn = 500
  n = 0
  an = a
  
  while n < maxn:
    
    n += 1
    einsatz += an
    gewinn = 2*an
    
    if np.random.uniform() < p:
      break
    else:
      an *= 2
      
  # print('E = %f, W = %f, n = %d' % (einsatz,gewinn,n) )
  return n, einsatz, gewinn


# Vektoren für die Statistik. Wir machen numsim Simulationen
numsim = 10000

nvec = np.zeros(numsim)
Evec = np.zeros(numsim)
Gvec = np.zeros(numsim)


for k in range(0,numsim):
  
  nvec[k],Evec[k],Gvec[k] = simulation(p,a)


maxrnd = 20
maxwin = a * 2**maxrnd


plt.figure()
plt.title('Anzahl Runden')
plt.hist(nvec,bins = maxrnd+1, range = (0,maxrnd))
  
    
# plt.figure()
# plt.title('Einsatz')
# plt.hist(Evec,bins = 40,range = (0,maxwin))
  
# plt.figure()
# plt.title('Gewinn')
# plt.hist(Evec,bins = 40,range = (0,maxwin))
    
print('Durchschnittliche Anzahl Runden = %f' % np.mean(nvec))  
print('Durchschnittlicher Einsatz = %f' % np.mean(Evec))  
print('Durchschnittlicher Gewinn = %f' % np.mean(Gvec))  
  
    
  
  
  
  



