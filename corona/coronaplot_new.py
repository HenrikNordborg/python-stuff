# -*- coding: utf-8 -*-
"""
Created on Wed Mar 25 06:48:20 2020

Simple analysis of the number of deaths from the Corona virus

The model assumes exponential growth where the coeffient changes 
linearly with time 

dN/dt = c(t) N(t)

with 

c(t) = c_0 + c_1 t

The data is taken from

https://data.humdata.org/dataset/novel-coronavirus-2019-ncov-cases

@author: Henrik

"""

import csv
import numpy as np
from datetime import datetime
import matplotlib.pyplot as plt
import requests

plt.rcParams.update({'font.size': 18})
deathcut = 10
tfitmax = 50

def read_deaths():
  
  url = ("https://data.humdata.org/hxlproxy/api/data-preview.csv?"
         "url=https%3A%2F%2Fraw.githubusercontent.com"
         "%2FCSSEGISandData%2FCOVID-19%2Fmaster%2Fcsse_covid_19_data"
         "%2Fcsse_covid_19_time_series%2Ftime_series_covid19_deaths_global.csv"
         "&filename=time_series_covid19_deaths_global.csv")
  
  # Read everything into the array data
  data = []
  # with open('time_series_covid19_deaths_global.csv') as File:
  #   reader = csv.reader(File)
  #   for row in reader:
  #     data.append(row)
  
  try:
    response = requests.get(url)
  except:
    print('Error: Unable to obtain data')
  
  reader = csv.reader(response.text.strip().split('\n'))
  for row in reader:
    data.append(row)

  # Convert the dates into ordinals. This is not strictly 
  # necessary.
  npts = len(data[0])

  for k in range(4,npts):
    tmp = datetime.strptime(data[0][k],'%m/%d/%y')
    data[0][k] = tmp.toordinal()

  return data

def read_confirmed():
  
  
  url = (" https://data.humdata.org/hxlproxy/api/data-preview.csv?"
         "url=https%3A%2F%2Fraw.githubusercontent.com"
         "%2FCSSEGISandData%2FCOVID-19%2Fmaster%2Fcsse_covid_19_data"
         "%2Fcsse_covid_19_time_series"
         "%2Ftime_series_covid19_confirmed_global.csv"
         "&filename=time_series_covid19_confirmed_global.csv" )
  
  # Read everything into the array data
  data = []
  # with open('time_series_covid19_deaths_global.csv') as File:
  #   reader = csv.reader(File)
  #   for row in reader:
  #     data.append(row)
  
  try:
    response = requests.get(url)
  except:
    print('Error: Unable to obtain data')
  
  reader = csv.reader(response.text.strip().split('\n'))
  for row in reader:
    data.append(row)

  # Convert the dates into ordinals. This is not strictly 
  # necessary.
  npts = len(data[0])

  for k in range(4,npts):
    tmp = datetime.strptime(data[0][k],'%m/%d/%y')
    data[0][k] = tmp.toordinal()

  return data


def plot_global_total( deaths, confirmed ):
  
  npts1 = len(deaths)
  dvec1 = np.array(deaths[0][4:],dtype = float)

  ddata = np.zeros( dvec1.shape )
  
  for k in range(1,npts1):
    ddata += np.array(deaths[k][4:],dtype = float)
    
  npts2 = len(confirmed)
  dvec2 = np.array(confirmed[0][4:],dtype = float)

  cdata = np.zeros( dvec2.shape )
  
  for k in range(1,npts2):
    cdata += np.array(confirmed[k][4:],dtype = float)
    

  if dvec1.size > dvec2.size: 
    dn = dvec2.size - dvec1.size
    cdata = cdata[dn:]
    
  dvec = np.zeros(dvec1.shape)
  for k in range(0,dvec1.size):
    dvec[k] = k
    
  mort = np.zeros(dvec.size)
  for k in range(0,mort.size):
    mort[k] = ddata[k]/cdata[k]
  
  
    
  plt.figure(10,figsize=(16,9))
  plt.title('Corona Deaths'  + ' - ' + date_str )
  plt.xlabel('days')
  plt.yscale('log')
  plt.grid()    
  
  labeltext = 'Deaths (%4.2e)' % ddata[-1]
  plt.plot(dvec,ddata,label=labeltext,color = 'r')
  labeltext = 'Cases (%4.2e)' % cdata[-1]
  plt.plot(dvec,cdata,label=labeltext,color = 'b')
  plt.legend()
  plt.show()
  
  plt.figure(11,figsize=(16,9))
  plt.title('Mortality'  + ' - ' + date_str )
  plt.ylabel('mort')
  plt.xlabel('days')
  plt.grid()    
  
  plt.plot(dvec,mort,label='Mortality',color = 'r')
  plt.legend()
  plt.show()
  
  return dvec1,ddata


def get_country( cname, data ):

  npts = len(data) - 1
  dvec = np.array(data[0][4:],dtype = float)
  
  # Extract data belonging to one country
  cnt = 1
  flag = 0
  while flag > -1:
    
    if data[cnt][1] == cname or cname == 'World':
      
      if flag == 0:
        
        deaths = np.array(data[cnt][4:],dtype = float)
        flag = 1
        
      elif flag == 1:
        
        deaths = deaths + np.array(data[cnt][4:],dtype = float)
      
    else:
      
      if flag == 1:
        flag = -1
        
    if cnt == npts:
      # print(data[cnt][1])
      break
    
    cnt += 1

    
  # Remove small values
  cnt = 0
  while deaths[cnt] < deathcut:
    cnt += 1
    
  dvec = dvec[cnt:]
  deaths = deaths[cnt:]
  
  return dvec,deaths


def polyval( c, x ):
  
  sum = ((c[3]*x + c[2])*x + c[1])*x + c[0]
  return sum       


def compute_diffs( data ):
  npts = data.shape[1]-1
  res = np.zeros((2,npts))
  for k in range (0,npts):
    res[0][k] = data[0][k+1]
    res[1][k] = data[1][k+1] - data[1][k]
  
  return res
  


def smart_fit( xdata, ydata ):
  
  npts = xdata.size
  x = np.arange( 0, npts-0.5, 1.0 )
  y = np.asarray( ydata )

  # Remove points that do not change

  cut = np.log(0.9)
  
  
  k = npts - 1

  if y[k] - y[k-1] < cut:
    
    print('========== WARNING CORRECTING DATA ===========')

    while y[k] - y[k-1] < cut:
      k -= 1
          
    npts = k+1
        
    x = x[0:npts]
    y = y[0:npts]

  
  fit_ok = False
  
  while not fit_ok:
    

    M = np.zeros((npts,3))
  
    for k in range(0,npts):
      
      M[k][0] = 1
      M[k][1] = x[k]
      M[k][2] = x[k]**2

    c = np.linalg.lstsq(M,y,rcond=None)[0]
       
    if c[2] < 0:
      tend = - c[1]/c[2]
      fit_ok = ( tend > x[-1])
    else:
      tend = x[-1]
      fit_ok = True
      
    if not fit_ok:
      npts -= 1
      x = x[0:npts]
      y = y[0:npts]

  return c, tend  

def linfit( x, y ):
  
  npts = x.size
  M = np.zeros((npts,2))

  for k in range(0,npts):
    M[k][0] = 1
    M[k][1] = x[k]
    
  c = np.linalg.lstsq(M,y,rcond=None)[0]
  return c

def polyfit( x1, y1, dy1, x2, y2, dy2 ):
  
  M = np.zeros((4,4))
  
  M[0] = (1, x1, x1**2, x1**3 )
  M[1] = (0, 1, 2*x1, 3*x1**2 )
  M[2] = (1, x2, x2**2, x2**3 )
  M[3] = (0, 1, 2*x2, 3*x2**2 )
    
  b = np.array((y1,dy1,y2,dy2))
  
  c = np.linalg.lstsq(M,b,rcond=None)[0]
  return c  

def lsqcond( A, y, cond, fc ):
  
  At = A.transpose()
  AA = np.dot(At,A)
  yp = np.dot(At,y)
  
  nrows = A.shape[0]
  ncoeffs = A.shape[1]
  ncond  = cond.shape[0]
  ntot = ncoeffs + ncond
  
  M = np.zeros((ntot,ntot))
  ym = np.zeros(ntot)
 
  M[0:ncoeffs,0:ncoeffs] = AA
  M[ncoeffs:ntot,0:ncoeffs] = cond
  M[0:ncoeffs,ncoeffs:ntot] = cond.transpose()
  ym[0:ncoeffs] = yp
  ym[ncoeffs:ntot] = fc
 
  c = np.linalg.solve(M,ym)
  cm = c[0:ncoeffs]
  
  return cm


def analyze_country( cname, data ):
  
  dvec,deaths = get_country( cname, data )
  
  currdate = datetime.fromordinal(int(dvec[-1])).strftime('%Y-%m-%d')
  currdeaths = deaths[-1]
  mytext =  'Current deaths: %d\n' % int(currdeaths)
  
  print( 'Country: %s (%s)' % ( cname.upper(), currdate ) )
  print( mytext )
  print('----')
   
 
  npts = len(dvec)

  y = np.zeros(npts)
  x = np.zeros(npts)
 
  for k in range(0,npts):
    x[k] = float(k)
    y[k]= np.log10( deaths[k] )
  
  # Fit the first points to a line
  ndays = 7
  nfit = npts

  xtmp = x[0:ndays]
  ytmp = y[0:ndays]  
  
  ca = np.polyfit(xtmp,ytmp,1)
  
  # Correct the time
  ldc = np.log10(deathcut)
  tfirst = ( ldc - ca[1] ) / ca[0]
  ca[1] = ldc  

  for k in range(0,npts):
    x[k] = x[k] - tfirst
    
  x1 = x[0]
  y1 = ca[1] + ca[0]*x1
  dy1 = ca[0]
 
  ndim = npts - 1
  
  res = np.zeros((3,ndim))

  for k in range(0,ndim):
    res[0][k] = x[k+1]
    res[1][k] = deaths[k+1] - deaths[k]
    
    
  for k in range(0,ndim):
    k1 = np.max((0,k-3))
    k2 = np.min((ndim,k+4))
    tmp = 0
    for kk in range(k1,k2):
      tmp += res[1][kk]
    res[2][k] = tmp/(k2 - k1)
      
  
  # mytext =  'Current deaths: %d\n' % int(currdeaths)
  # mytext += 'Daily deaths: %d\n' % int(daily_chg)
  # mytext += 'Initial rate: %4.4f\n' % initial_rate
  # mytext += 'Current rate: %4.4f\n' % curr_rate
  # mytext += 'Doubling time: %4.2f\n' % tdbl
  
 
  # print('Initial rate of increase %4.4f' % initial_rate)
  # print('Current rate of increase %4.4f' % curr_rate)
  # print('Time to double %4.4f' % tdbl)
  # print('Duration %4.4f' % x[-1])
  # print('Current deaths: %d'% int(currdeaths) )
  # print('Daily change %d' % int(daily_chg))

  titletext = '%s %s - current deaths %d (%d)' % (cname,currdate, \
              currdeaths,res[1][-1])
    
  plt.figure(figsize=(16,9))
  plt.title(titletext)
  plt.grid()
  plt.plot(res[0],res[1],'o',label='deaths')
  plt.plot(res[0],res[2],'-',label='fit')  
  plt.xlabel('Daily deaths')
  #plt.text(posx,posy,mytext)
  plt.show() 
  
  
  return res
  


# ------------------------
# Main Program
# ------------------------
 
data = read_deaths()
#data = read_confirmed()

last_date = datetime.fromordinal( int(data[0][-1]) )
date_str = last_date.strftime('%b %d %Y')

fr = analyze_country( 'France', data )
us = analyze_country( 'US', data )
it = analyze_country( 'Italy', data )
sp = analyze_country( 'Spain', data )
ch = analyze_country( 'Switzerland', data )
uk = analyze_country( 'United Kingdom', data )
de = analyze_country( 'Germany', data )
se = analyze_country( 'Sweden', data )
au = analyze_country( 'Austria', data )
be = analyze_country( 'Belgium', data )
#cn,cnfit = analyze_country( 'China', data )
ne = analyze_country( 'Netherlands', data )
br = analyze_country( 'Brazil', data )
mx = analyze_country( 'Mexico', data )
wd = analyze_country( 'World', data )



